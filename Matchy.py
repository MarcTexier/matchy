import tkinter as tk 
from tkinter import font  as tkfont 
from tkinter import *
import tkinter.messagebox
import json


class SampleApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.title_font = tkfont.Font(family='Courrier', size=40, weight="bold", slant="italic")
        self.geometry("1080x720")

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartPage, Inscription, Connexion):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("StartPage")

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Matchy", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)

        button1 = tk.Button(self, text="Inscription", bg='blue', font=("courrier", 35), command=lambda: controller.show_frame("Inscription"))
        button2 = tk.Button(self, text="Connexion", bg='red', font=("courrier", 34), command=lambda: controller.show_frame("Connexion"))
        button1.pack()
        button2.pack()

def envoyer(Champ, Prenom, Age, Mail, MDP, MDP2, Commentaire, c, e, p, m, t, r):
    user = {
        "Nom": Entry(Champ).get(),
        "Prenom": Prenom,
        "Age": Age,
        "Mail": Mail,
        "MDP": MDP,
        "MDP2": MDP2,
        "Commentaire": Commentaire,
    }

    with open("data.json", "r") as f:
        list_users = json.load(f)
    list_users.append(user)
    with open("data.json", "w") as f:
        json.dump(list_users, f)
    

class Inscription(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text="Inscription", font=controller.title_font)
        label.grid(column=4, row=0, sticky='w',pady=2)

        self.grid(row=0, column=0, sticky=(N, W, E, S))

        Nom = tk.Label(self, font=("courrier", 15), text = 'Nom : ')
        Nom.grid(column=0, row=0, sticky='w',pady=2)
        #Nom.pack()

        Nom=StringVar()
        Nom = tk.Entry(self, textvariable= Nom, width=35)
        Nom.grid(column=1, row=0, sticky='sw', columnspan=2, padx=10)
        #Champ.pack()

        Prenom = tk.Label(self, font=("courrier", 15), text = 'Prénom : ',)
        Prenom.grid(column=0, row=1,sticky='w',pady=2)
        #Prenom.pack()

        Prenom=StringVar()
        Champ2 = tk.Entry(self, textvariable= Prenom, width=35)
        Champ2.grid(column=1, row=1,columnspan=2)
        #Champ2.pack()

        Age = Label(self, font=("courrier", 15), text = 'Age : ')
        #Age.pack()
        Age.grid(column=0, row=2, sticky='w',pady=2)
        Age=StringVar()
        Champ3 = Entry(self, textvariable= Age,width=35)
        Champ3.grid(column=1, row=2,columnspan=2)
        #Champ3.pack()
        
        
        Mail = Label(self, font=("courrier", 15), text = 'Adresse mail : ')
        #Mail.pack()
        Mail.grid(column=0, row=3,sticky='w',pady=2)
        Mail=StringVar()
        Champ4 = Entry(self, textvariable= Mail,width=35)
        Champ4.grid(column=1, row=3,columnspan=2)
        #Champ4.pack()
        
        
        MDP = Label(self, font=("courrier", 15), text = 'Mot de passe : ')
        #MDP.pack()
        MDP.grid(column=0, row=4,sticky='w', pady=2)
        MDP=StringVar()
        Champ5 = Entry(self, textvariable= MDP,width=35, show='*')
        Champ5.grid(column=1, row=4,columnspan=2)
        #Champ5.pack()
        
        
        MDP2 = Label(self, font=("courrier", 15), text = 'Retapez votre mot de passe : ')
        #MDP2.pack()
        MDP2.grid(column=0, row=5, sticky='w',pady=2)
        MDP2=StringVar()
        Champ6 = Entry(self, textvariable= MDP2,width=35, show='*')
        Champ6.grid(column=1, row=5,columnspan=2)
        #Champ6.pack()
        
        
        Commentaires = Label(self, font=("courrier", 15), text = 'Commentaires : ')
        #Commentaires.pack()
        Commentaires.grid(column=0,row=6, sticky='w',pady=2)
        Commentaires=StringVar()
        Champ7 = Entry(self, textvariable= Commentaires,width=35)
        Champ7.grid(column=1, row=6, ipady=25,columnspan=2)
        #Champ7.pack()
        
        
        Sexe = Label(self, font=("courrier", 15), text = 'Sexe : ')
        Sexe.grid(column=0,row=7, sticky='w',pady=2)
        #Sexe.pack()
        
        sex=IntVar()
        
        homme= Radiobutton (self, font=("courrier", 15), text="Homme", variable=sex, value=1)
        #homme.pack()
        homme.grid(column=1, row=7,sticky='sw')
        femme= Radiobutton (self, font=("courrier", 15), text="Femme", variable=sex, value=2)
        #femme.pack()
        femme.grid(column=2, row=7,sticky='sw')
        
        Hobbies = Label(self, font=("courrier", 15), text = 'Hobbies : ')
        #Hobbies.pack()
        Hobbies.grid(column=0,row=9, sticky='w',pady=2)
        
        c=IntVar()
        e=IntVar()
        p=IntVar()
        m=IntVar()
        t=IntVar()
        r=IntVar()
        
        C1= tk.Checkbutton(self, font=("courrier", 15), text="Cinema", variable=c, onvalue=1, offvalue=0)
        C1.grid (column=1, row=8,sticky='sw')
        #C1.pack()
        
        C2= Checkbutton (self, font=("courrier", 15), text="Equitation", variable=e, onvalue=2, offvalue=0)
        C2.grid (column=1, row=9, sticky='sw')
        #C2.pack()
        
        C3= Checkbutton (self, font=("courrier", 15), text="Planche à voile", variable=p, onvalue=3, offvalue=0)
        C3.grid (column=1, row=10, sticky='sw')
        #C3.pack()
        
        C4= Checkbutton (self, font=("courrier", 15), text="Musique", variable=m, onvalue=4, offvalue=0)
        C4.grid (column=2, row=8,sticky='sw')
        #C4.pack()
        
        C5= Checkbutton (self, font=("courrier", 15), text="Theatre", variable=t, onvalue=5, offvalue=0)
        C5.grid (column=2, row=9, sticky='sw')
        #C5.pack()
        
        C6= Checkbutton (self, font=("courrier", 15), text="Rien", variable=r, onvalue=6, offvalue=0)
        C6.grid (column=2, row=10, sticky='sw')
        #C6.pack()
        
        Envoyer= Button (self, text="envoyer",command= envoyer(Nom.get(), Prenom.get(), Age.get(), Mail.get(), MDP.get(), MDP2.get(), Commentaires.get(), c.get(), e.get(), p.get(), m.get(), t.get(), r.get()), pady=2)
        Envoyer.grid (column=1, row=11,sticky='sw', pady=20)
        Effacer= Button (self, text="réeinitialiser", command=effacer, pady=2)
        Effacer.grid (column=2, row=11,sticky='sw',pady=20)

        button = tk.Button(self, text="Retour", bg='Black', font=("courrier", 35), fg="White", command=lambda: controller.show_frame("StartPage"))
        button.grid(column=4, row=10, sticky='w',pady=2)

def effacer():
    Champ.delete(0,END)
    Champ2.delete(0,END)
    Champ3.delete(0,END)
    Champ4.delete(0,END)
    Champ5.delete(0,END)
    Champ6.delete(0,END)
    Champ7.delete(0,END)
    C1.deselect()
    C2.deselect()
    C3.deselect()
    C4.deselect()
    C5.deselect()
    C6.deselect()
    homme.deselect()
    femme.deselect()

class Connexion(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Connexion", font=controller.title_font)
        label.grid(column=4, row=0, sticky='w',pady=2)
        button = tk.Button(self, text="Retour", bg='Black', font=("courrier", 35), fg="White", command=lambda: controller.show_frame("StartPage"))
        button.grid(column=4, row=10, sticky='w',pady=2)


        Email = tk.Label(self, font=("courrier", 15), text = 'Email : ')
        Email.grid(column=0, row=0, sticky='w',pady=2)
            #Nom.pack()

        Email=StringVar()
        Champ = tk.Entry(self, textvariable= Email, width=35)
        Champ.grid(column=1, row=0, sticky='sw', columnspan=2, padx=10)
            #Champ.pack()

        password = tk.Label(self, font=("courrier", 15), text = 'Mot de passe : ',)
        password.grid(column=0, row=1,sticky='w',pady=2)
            #Prenom.pack()

        password = StringVar()
        Champ2 = tk.Entry(self, textvariable= password, width=35)
        Champ2.grid(column=1, row=1,columnspan=2)
        #Champ2.pack()


if __name__ == "__main__":
    app = SampleApp()
    app.mainloop()